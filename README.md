# lsqfit-examples

This repo contains usage examples of `lsqfit`. In particular, this notebook uses the `Multifitter` class to fit a pair of baryon (or meson) correlators, each correlator corresponding to a different sink; and then, using `lsqfit`'s build-in method for bootstrapping, this notebook generates bootstrap samples of the fit parameters.

Run _mwe.ipynb_ to get started.

## Requirements

* [python 2.7](https://www.python.org/) (in principle, python 3 should also work with slight modifications)
* [Jupyter notebook](https://github.com/jupyter/notebook) (alternatively, copy and paste notebook contents into a .py file)
*  [gvar](https://github.com/gplepage/gvar) (at least version v 9.0.0: this release better supports log-normally distributed `gvar` variables in `gvar.Bufferdict`s by automatically creating an entry for `x` whenever an entry for `log(x)` is created)
*  [lsqfit](https://github.com/gplepage/lsqfit)
*  [h5py](https://github.com/h5py/h5py)
